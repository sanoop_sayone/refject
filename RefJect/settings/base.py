"""
Django settings for RefJect project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
gettext = lambda s: s
# import dj_database_url
# DATABASES = {}
#DATABASES['default'] =  dj_database_url.config()


BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_DIR = os.path.abspath(os.path.join(BASE_DIR, os.path.pardir))
PROJECT_PATH = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0]
SUPER_DIR = os.path.abspath(os.path.join(PROJECT_DIR, os.path.pardir))
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '30+r70$-hel6#3qo8z96_x%k5b2-^0u3tnk_x7wz^v%trj$3q+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

SITE_ID = 2

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']

ADMIN_EMAIL = 'sanooppk7@gmail.com'

TEMPLATE_DIRS = (

    os.path.join(PROJECT_DIR, "templates"),
)

CMS_TEMPLATES = (
    ('index.html', 'index'),
    ('about.html', 'about'),
    ('contactus.html', 'contact'),
    ('login.html', 'login'),
)
CMS_PLACEHOLDER_CONF={
        'plugin_content':{
            "plugins":('ContactPlugin'),
             "name":gettext('Contact Plugin')

        },}

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'mailer',
    'cms',  # django CMS itself
    'mptt',
    'treebeard',  # utilities for implementing a tree
    'menus',  # helper for model independent hierarchical website navigation
    'sekizai',  # for javascript and css management
    'djangocms_admin_style',  # for the admin skin. You **must** add 'djangocms_admin_style' in the list **before** 'django.contrib.admin'.
    'applications.contact',
    'applications.site_settings',
    'applications.footer_links',
    'django_extensions',
    'newsletter',
    'sorl.thumbnail',
    'xmlsitemap',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.google',
    'captcha',
    )

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    
)

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",
    # `allauth` specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend"
)


TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'sekizai.context_processors.sekizai',
    'cms.context_processors.cms_settings',
    'applications.footer_links.context_processor.footer_settings',
)

MIGRATION_MODULES = {
    # Add also the following modules if you're using these plugins:
    'djangocms_file': 'djangocms_file.migrations_django',
    'djangocms_flash': 'djangocms_flash.migrations_django',
    'djangocms_googlemap': 'djangocms_googlemap.migrations_django',
    'djangocms_inherit': 'djangocms_inherit.migrations_django',
    'djangocms_link': 'djangocms_link.migrations_django',
    'djangocms_picture': 'djangocms_picture.migrations_django',
    'djangocms_snippet': 'djangocms_snippet.migrations_django',
    'djangocms_teaser': 'djangocms_teaser.migrations_django',
    'djangocms_video': 'djangocms_video.migrations_django',
    # 'djangocms_text_ckeditor': 'djangocms_text_ckeditor.migrations_django',
}

LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/login'

FACEBOOK_APP_ID = '494933210670684'
FACEBOOK_API_SECRET = '0c1856286734d06b8117ad48674f0278'

GOOGLE_OAUTH2_CLIENT_ID = '890951887777-rk90bolg5uq0chl19j7s837df7v2tp9m.apps.googleusercontent.com'
GOOGLE_OAUTH2_CLIENT_SECRET = 'eLuS9V0I8spz9WGu-z5eZXZK'





ROOT_URLCONF = 'RefJect.urls'

WSGI_APPLICATION = 'RefJect.wsgi.application'

LANGUAGES = [
    ('en', 'English'),
]

NEWSLETTER_CONFIRM_EMAIL = True
NEWSLETTER_CONFIRM_EMAIL_SUBSCRIBE = True


EMAIL_BACKEND = 'mailer.backend.DbBackend'

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'itsdonearun@gmail.com'
EMAIL_HOST_PASSWORD = 'itsdonearun93'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'itsdonearun@gmail.com'


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en'

TIME_ZONE = 'Asia/Kolkata'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

CKEDITOR_UPLOAD_PATH = "uploads/"

CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'

CKEDITOR_CONFIGS = {
  'default': {
      'toolbar': 'Advanced',
  },
}


STATIC_ROOT = os.path.join(PROJECT_DIR, "static")
STATIC_URL = "/static/"
# LOCALE_PATHS = (os.path.join(PROJECT_DIR, "locale"),)

# Additional locations of static files
STATICFILES_DIRS = (os.path.join(PROJECT_DIR, 'staticfiles'),)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'compressor.finders.CompressorFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)


MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = "/media/"

# captcha settings
RECAPTCHA_PUBLIC_KEY = '6LcvGgoTAAAAAMep3q_OaieWYRQpAuTZIY3YxSFp'
RECAPTCHA_PRIVATE_KEY = '6LcvGgoTAAAAADd5Bga3nZVkqSdO5LRvtHghC49R'
RECAPTCHA_USE_SSL = True


CMS_PLACEHOLDER_CONF={
        'plugin_content':{
            "plugins":('ContactPlugin'),
             "name":gettext('Contact Plugin')

        },
        'default_plugins':[
            {
                'plugin_type':'TextPlugin',
                'values':{
                    'body':'<p>Great websites : %(_tag_child_1)s and %(_tag_child_2)s</p>'
                },
                'children':[
                    {
                        'plugin_type':'LinkPlugin',
                        'values':{
                            'name':'django',
                            'url':'https://www.djangoproject.com/'
                        },
                    },
                    {
                        'plugin_type':'LinkPlugin',
                        'values':{
                            'name':'django-cms',
                            'url':'https://www.django-cms.org'
                        },
                        # If using LinkPlugin from djangocms-link which
                        # accepts children, you could add some grandchildren :
                        # 'children' : [
                        #     ...
                        # ]
                    },
                ]
                }
            ]
        }

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
# ALLOWED_HOSTS = ['*']

CMS_PLUGIN_CACHE = False

DATABASES = {}
