from django.conf import settings
from django.conf.urls import include, url, patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.http import HttpResponse


admin.autodiscover() # Not required for Django 1.7.x+

urlpatterns = patterns('',
	# url(r'^contact_us/',include('applications.contact.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^newsletter/', include('newsletter.urls')),
    url(r'^login/',include('applications.useraccount.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
    url(r'^', include('cms.urls')),
    

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += patterns('',
        url(r'^', include('xmlsitemap.urls')),
        (r'^robots.txt$', lambda r: HttpResponse("User-agent: *\nDisallow: /", content_type='application/json')),

    )
