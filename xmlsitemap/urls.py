from django.conf.urls import patterns,url

from xmlsitemap.conf import OTHER_SITEMAPS
from xmlsitemap.models import XMLSitemap
from xmlsitemap.sitemaps import ModelSitemap
from django.contrib.sitemaps.views import sitemap
modelsitemaps = {
'pages' : ModelSitemap(XMLSitemap.get_active_siteurls()),
}
modelsitemaps.update(OTHER_SITEMAPS)

urlpatterns = patterns('django.contrib.sitemaps.views',
    (r'^sitemap\.xml$', 'sitemap', {'sitemaps': modelsitemaps}),
)