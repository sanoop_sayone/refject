from django.contrib import admin
from models import *

class Listfooterlink(admin.ModelAdmin):
    list_display=['title','sort_order',]
    list_editable = ('sort_order',)
admin.site.register(Footerlinks,Listfooterlink)