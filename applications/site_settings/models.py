from django.db import models
from django.db import models
from cms.models import Site
from django.db.models.signals import post_save

from datetime import date


class SiteSetting(models.Model):

    sites = models.OneToOneField(Site, unique=True)
    email = models.EmailField('Email')

    def __unicode__(self):
        return u'%s' % (self.email)

    class Meta:
        verbose_name_plural = "site settings"
