from django import forms
from applications.contact.models import contact
from captcha.fields import ReCaptchaField
from django.utils.translation import ugettext as _

class ContactForm(forms.ModelForm):
    # captcha=ReCaptchaField()

    class Meta:
        model=contact
        widgets = {'message':forms.Textarea}

    def __init__(self,*args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['placeholder']='Name*'
        self.fields['subject'].widget.attrs['placeholder']='Subject*'
        self.fields['email'].widget.attrs['placeholder']='Email Address*'
        self.fields['phone'].widget.attrs['placeholder']='Phone Number*'
        self.fields['message'].widget.attrs['placeholder']='Message*'
        self.fields['name'].widget.attrs['class']='validate[required] input-large'
        self.fields['subject'].widget.attrs['class']='validate[required] input-large'
        self.fields['email'].widget.attrs['class']='validate[required,custom[email]] input-large'
        self.fields['phone'].widget.attrs['class']='input-large validate[custom[phone]]'
        self.fields['message'].widget.attrs['class']='validate[required] input-large'
          

def clean_phone(self):
    phone=self.cleaned_data['phone']
    try:
        mob=int(phone)
        return mob
    except:
        raise forms.ValidationError(_("Please enter valid number"))