# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0011_auto_20150419_1006'),
    ]

    operations = [
        migrations.CreateModel(
            name='Footerlinks',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=300, null=True, verbose_name='Title', blank=True)),
                ('URL', models.URLField(help_text='If page is not specified, footer link will redirect to given Url', null=True, verbose_name='URL', blank=True)),
                ('sort_order', models.IntegerField(default=0, verbose_name='SORT_ORDER')),
                ('display', models.BooleanField(default=True, verbose_name='Display')),
                ('page_link', models.ForeignKey(blank=True, to='cms.Page', help_text='If specified,This page will appear in the footer link', null=True, verbose_name='page')),
            ],
            options={
                'ordering': ('sort_order',),
                'verbose_name': 'Footer Link',
                'verbose_name_plural': 'Footer Links',
            },
            bases=(models.Model,),
        ),
    ]
