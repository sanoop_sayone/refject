from cms.plugin_base import CMSPluginBase
from django.utils.translation import ugettext_lazy as _
from cms.plugin_pool import plugin_pool
from applications.contact.models import ContactUsPlugin
from applications.contact.forms import ContactForm
from django.forms.util import ErrorList
from mailer import send_mail
from django.conf import settings
from django.template import Context
from django.template import loader
from django.contrib.sites.models import Site


class DivErrorList(ErrorList):
  def __unicode__(self):
    return self.as_divs()

  def as_divs(self):
    if not self: return u''
    return u'<div class="errorlist">%s</div>' % ''.join([u'<div class="error">%s</div>' % e for e in self])


class ContactPlugin(CMSPluginBase):
  model = ContactUsPlugin
  name = "Contact Form"
  render_template = "contact.html"

  def render(self, context, instance, placeholder):
    status = False
    request = context['request']
    current_site = Site.objects.get_current()
    email_to = "arun.sayone@gmail.com"
    if request.method == "POST":
      form = ContactForm(request.POST, auto_id=False)
      if form.is_valid():
        form.save()
        email = request.POST['email']
        message = request.POST['message']
        t = loader.get_template("contact_request.txt")
        c = Context({'email': email, 'site': current_site, 'message': message, })
        send_mail('[%s] %s' % (current_site.name, 'New Contact Request'), t.render(c), settings.DEFAULT_FROM_EMAIL,
                  [email_to], fail_silently=False)
        form = ContactForm(auto_id=False)
        status = True
        context.update({
          'contact': instance,
          'form': form,
          'status': status,
          'success': _('Your contact request has been successfully sent.We will contact you soon.')
        })
        return context
      else:
        context.update({
          'contact': instance,
          'form': form,
          'status': status,
        })
    else:
      form = ContactForm(auto_id=False)

    context.update({
      'contact': instance,
      'form': form,
      'status': status,
    })
    return context


plugin_pool.register_plugin(ContactPlugin)

