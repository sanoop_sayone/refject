from django.db import models
from cms.models import CMSPlugin
from django_extensions.db.models import TimeStampedModel
from django.utils.translation import ugettext_lazy as _

class contact(TimeStampedModel):
    name=models.CharField(_("Name"),max_length=100)
    subject=models.CharField(_("Subject"),max_length=100)
    email=models.EmailField(_("Email Address"),max_length=100)
    phone=models.BigIntegerField(_("Phone"),max_length=20,blank=True,null=True)
    message=models.CharField(_("Message"),max_length=200)
# Create your models here.
    def __unicode__(self):
        return self.name
class Meta:
    verbose_name=("contact")
    verbose_name_plural=_("contact")

class ContactUsPlugin(CMSPlugin):
    h_name= models.CharField(_("Headquarter Name"), max_length=255,null=True, blank=True)
    h_area=models.CharField(_("Headquarter Area"), max_length=255,null=True, blank=True)
    h_street=models.CharField(_("Headquarter Street"), max_length=255,null=True, blank=True)
    h_location=models.CharField(_("Headquarter location"), max_length=255,null=True, blank=True)
    h_phone= models.CharField(_("Headquarter Phone"), max_length=255,null=True, blank=True)
    h_fax= models.CharField(_("Headquarter Fax"), max_length=255,null=True, blank=True)
    h_email = models.EmailField(_("Headquarter Email"), max_length=255,null=True, blank=True)

    latitude=models.DecimalField(_("Google Map Latitude"),max_digits=25,decimal_places=6)
    longitude=models.DecimalField(_("Google Map Longitude"),max_digits=25,decimal_places=6)

