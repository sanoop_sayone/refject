from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import Page
from django.contrib.sites.models import Site


class Footerlinks(models.Model):
    title = models.CharField(_("Title"), max_length=300, null=True, blank=True)
    page_link = models.ForeignKey(
        Page,
        verbose_name=_("page"),
        blank=True,
        null=True,
        limit_choices_to={'publisher_is_draft': True},help_text=_('If specified,This page will appear in the footer link')
    )
    URL = models.URLField(_("URL"), blank=True,null=True,help_text=_('If page is not specified, footer link will redirect to given Url'))
    sort_order = models.IntegerField(_("SORT_ORDER"), default=0)
    display = models.BooleanField(_("Display"), default=True)
    class Meta:
        verbose_name = _("Footer Link")
        verbose_name_plural = _("Footer Links")
        ordering = ('sort_order',)

    def __unicode__(self):
        return "%s" % self.title