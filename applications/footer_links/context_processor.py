from applications.footer_links.models import Footerlinks


def footer_settings(request):
    footerlinks = Footerlinks.objects.filter(display=True)
    ctx = {
        'footerlinks': footerlinks,
        }
    return ctx