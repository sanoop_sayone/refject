# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0011_auto_20150419_1006'),
    ]

    operations = [
        migrations.CreateModel(
            name='contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', django_extensions.db.fields.CreationDateTimeField(default=django.utils.timezone.now, verbose_name='created', editable=False, blank=True)),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(default=django.utils.timezone.now, verbose_name='modified', editable=False, blank=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('subject', models.CharField(max_length=100, verbose_name='Subject')),
                ('email', models.EmailField(max_length=100, verbose_name='Email Address')),
                ('phone', models.BigIntegerField(max_length=20, null=True, verbose_name='Phone', blank=True)),
                ('message', models.CharField(max_length=200, verbose_name='Message')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ContactUsPlugin',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin')),
                ('h_name', models.CharField(max_length=255, null=True, verbose_name='Headquarter Name', blank=True)),
                ('h_area', models.CharField(max_length=255, null=True, verbose_name='Headquarter Area', blank=True)),
                ('h_street', models.CharField(max_length=255, null=True, verbose_name='Headquarter Street', blank=True)),
                ('h_location', models.CharField(max_length=255, null=True, verbose_name='Headquarter location', blank=True)),
                ('h_phone', models.CharField(max_length=255, null=True, verbose_name='Headquarter Phone', blank=True)),
                ('h_fax', models.CharField(max_length=255, null=True, verbose_name='Headquarter Fax', blank=True)),
                ('h_email', models.EmailField(max_length=255, null=True, verbose_name='Headquarter Email', blank=True)),
                ('latitude', models.DecimalField(verbose_name='Google Map Latitude', max_digits=25, decimal_places=6)),
                ('longitude', models.DecimalField(verbose_name='Google Map Longitude', max_digits=25, decimal_places=6)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
