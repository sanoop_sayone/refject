# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('site_settings', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sitesetting',
            options={'verbose_name_plural': 'site settings'},
        ),
        migrations.AlterField(
            model_name='sitesetting',
            name='email',
            field=models.EmailField(max_length=75, verbose_name=b'Email'),
            preserve_default=True,
        ),
    ]
